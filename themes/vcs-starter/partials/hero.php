<?php
    $image = get_field('hb_background_image');
    //dump($image);
    ?>
<section class="hero" style="background-image: url(<?php echo $image['sizes']['1536x1536']; ?>);">
    <div class="hero-background">
        <div class="header">
        <a href="#" class="logo">
            <?php
            $image = get_field('ho_logo_image', 'option');
            ?>
            <img src="<?php echo $image ['sizes']['logo']; ?>" alt="<?php bloginfo('name'); ?>"> 
        </a>
        </div>
        <div class="container flex">
            <div>
                <h1><?php the_field('hb_heading'); ?></h1>
                <p><?php the_field('hb_description'); ?></p>
                <?php $link=get_field('hb_link');
                $target = $link['target'] ? ' target="_blank" ' : '';
                ?>
                <a href="<?php echo link['url'] ?>" <?php echo $target; ?> class="btn"><?php echo $link['title']; ?></a>
            </div>
            <div class="phone">
            <?php
            $image = get_field('hb_phone');
            ?>
            <img src="<?php echo $image ['sizes']['hb_phone']; ?>" alt="<?php bloginfo('name'); ?>"> 
                <div class="dexter_phone">
                <?php
                $image = get_field('hb_phone_character');
                ?>
                <img src="<?php echo $image ['sizes']['hb_phone_character']; ?>" alt="<?php bloginfo('name'); ?>"> 
                </div>
            </div>
        </div>
        <div class= "blue-background">
            <p><?php the_field('hb_comment'); ?></p>
        </div>
    </div>
</section>