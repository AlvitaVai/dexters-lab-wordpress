<footer class="fifth-block">
			<div class="container">
				<div class="section-content">
					<h2><?php the_field('hf_heading') ?></h2>
				</div>
			</div>
			<div class= "darker-background">
			<div class= "container flex">
            <?php
            // check if the repeater field has rows of data
            if( have_rows('hf_contacts_repeater') ):
               // loop through the rows of data
                while ( have_rows('hf_contacts_repeater') ) : the_row();
                ?>
                <div class="column flex">
					<div class="icon">
					<?php the_sub_field('hf_icon'); ?>
					</div>
					<div class="contacts">
					<div><?php the_sub_field('hf_main_contact'); ?></div>
					</div>
				</div>
                    <?php
                endwhile;
            endif;
            ?>		
			</div>	
			<div class="BB flex">
                <?php
                $image = get_field('hf_brand');
                ?>
                <img src="<?php echo $image ['sizes']['hf_brand']; ?>" alt="<?php bloginfo('name'); ?>">                
			</div>		
			</div>
        </footer>
        <?php wp_footer(); ?>
	</body>
</html>