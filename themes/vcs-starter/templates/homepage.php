<?php

/* Template Name: Homepage Template */

get_header();

get_template_part('partials/hero');
get_template_part('partials/features');
get_template_part('partials/main content');
get_template_part('partials/characters');
?>

<!-- Start Point -->

<!-- <h1>homepage.php</h1> -->

<?php get_footer(); ?>