<section class="third-block">
    <div class="container">
        <div class="third_section_content">
            <div class="flex">
                <div class="character1">
                    <?php
                    $image = get_field('mc_first_character');
                    ?>
                    <img src="<?php echo $image ['sizes']['mc_first_character']; ?>" alt="<?php bloginfo('name'); ?>"> 
                </div>
                <div class="Ginger-Description">
                    <div>
                        <h2><?php the_field('mc_heading'); ?></h2>
                        <p><?php the_field('mc_sub_heading'); ?></p>
                    </div>
                <div class="Intro">
                    <div class = "Mandark-description">
                    <p><?php the_field('mc_description'); ?></p>
                    </div>
                    <div class="overlay">
                        <?php
                        $image = get_field('mc_small_character');
                        ?>
                        <img src="<?php echo $image ['sizes']['mc_small_character']; ?>" alt="<?php bloginfo('name'); ?>"> 
                    </div>
                </div>
                    <div class="Mandark">
                        <p><?php the_field('mc_ending'); ?></p>
                    </div>
                </div>
            </div>
            <div class="flex">
                <div class="Dexter">
                    <div>
                        <h2><?php the_field('mc_second_heading'); ?></h2>
                        <p><?php the_field('mc_second_sub_heading'); ?></p>
                        <?php $link=get_field('hb_link');
                        $target = $link['target'] ? ' target="_blank" ' : '';
                        ?>
                        <a href="<?php echo link['url'] ?>" <?php echo $target; ?> class="btn"><?php echo $link['title']; ?></a>
                    </div>
                </div>
                    <div class="character2">
                        <?php
                        $image = get_field('mc_second_character');
                        ?>
                        <img src="<?php echo $image ['sizes']['mc_second_character']; ?>" alt="<?php bloginfo('name'); ?>"> 
                    </div>				
            </div>
        </div>
    </div>
</section>