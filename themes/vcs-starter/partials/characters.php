<section class="fourth-block">
    <div class="container">
        <div class="section-content flex">
        <?php
            // check if the repeater field has rows of data
            if( have_rows('hcd_characters') ):
                // loop through the rows of data
                while ( have_rows('hcd_characters') ) : the_row();
                    ?>
                    <div class="column">
                    <?php
                    $image = get_sub_field('hcd_image');
                    // dump($image);
                    ?>
                    <img src="<?php echo $image ['sizes']['hcd_image']; ?>" alt="<?php bloginfo('name'); ?>"> 
                    <div><h1><?php the_sub_field('hcd_heading'); ?></h1></div>
                        <div><h2><?php the_sub_field('hcd_title'); ?></h2></div>
                        <div class="content">
                            <span><?php the_sub_field('hcd_text'); ?></span>
                            <button class="btn">+more</button>
                            <span class="hidden-content"><?php the_sub_field('hcd_additional_text'); ?></span> 
                        </div>			
                    </div>
                    <?php
                endwhile;
            endif;
            ?>
        </div>
    </div>
</section>