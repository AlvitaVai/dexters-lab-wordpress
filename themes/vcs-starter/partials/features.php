<section class="second-block">
    <div class="container">
        <div class="section-content flex">
        <?php
            // check if the repeater field has rows of data
            if( have_rows('hf_features_repeater') ):
                // loop through the rows of data
                while ( have_rows('hf_features_repeater') ) : the_row();
                    ?>
                    <div class="column">
                        <?php
                        $image = get_sub_field('image');
                        // dump($image);
                        ?>
                        <img src="<?php echo $image ['sizes']['featuresimage']; ?>" alt="<?php bloginfo('name'); ?>"> 
                        <h2><?php the_sub_field('title'); ?></h2>
                        <p><?php the_sub_field('description'); ?></p>
                    </div>
                    <?php
                endwhile;
            endif;
            ?>
        </div>
    </div>
</section>