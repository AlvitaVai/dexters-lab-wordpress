<?php

//IMG sizes

add_image_size('logo', 160, 31, false);
add_image_size('hb_phone_character', 250, 450, false);
add_image_size('hb_phone', 371, 782, false);
add_image_size('featuresimage', 143, 157, false);
add_image_size('mc_first_character', 300, 350, false);
add_image_size('mc_small_character', 105, 105, false);
add_image_size('mc_second_character', 500, 520, false);
add_image_size('hcd_image', 300, 220, false);
add_image_size('hf_brand', 130, 85, false);


// Turn on post thumbnail

add_theme_support( 'post-thumbnails' );

// Define theme styles

if( !defined('THEME_FOLDER') ) {
	define('THEME_FOLDER', get_bloginfo('template_url'));
}

function theme_scripts(){

    if ( !is_admin() ) {

		//wp_register_script(handle, path, dependency, version, load_in_footer);

        //wp_deregister_script('jquery');
		//wp_register_script('jquery', THEME_FOLDER . '/assets/js/jquery-2.1.1.js', false, '2.1.1', true);

		//Registration
		

        wp_register_script(
			'fa', 
			"https://kit.fontawesome.com/113357d0d3.js", 
			array(), 
			false, 
			false
		);


		wp_register_script(
			'scripts', 
			THEME_FOLDER . '/assets/scripts/custom.js', 
			array('fa'), 
			false, 
			true
		);



        //Loading
		wp_enqueue_script('fa');
		wp_enqueue_script('scripts');
    }
}
add_action('wp_enqueue_scripts', 'theme_scripts');


function theme_stylesheets(){

	if( defined('THEME_FOLDER') ) {
		//wp_register_style(handle, path, dependency, version, devices);		
		
		wp_register_style(
			'fonts', 
			"https://fonts.googleapis.com/css?family=Asap:400,400i,700&display=swap&subset=latin-ext", 
			array(), 
			false, 
			'all'
		);


		//Registration
		wp_register_style(
			'style', 
			THEME_FOLDER . '/assets/css/style.css', 
			array('fonts'), 
			false, 
			'all'
		);

		//Loading
		wp_enqueue_style('fonts');
		wp_enqueue_style('style');
	}
}
add_action('wp_enqueue_scripts', 'theme_stylesheets');

// Navigation

function register_theme_menus() {
   
	register_nav_menus(array( 
        'primary-navigation' => __( 'Primary Navigation' ) 
    ));
}

add_action( 'init', 'register_theme_menus' );

// Sidebars

#$sidebars = array( 'Footer Widgets', 'Blog Widgets' );

if( isset($sidebars) && !empty($sidebars) ) {

	foreach( $sidebars as $sidebar ) {

		if( empty($sidebar) ) continue;

		$id = sanitize_title($sidebar);

		register_sidebar(array(
			'name' => $sidebar,
			'id' => $id,
			'description' => $sidebar,
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>'
		));
	}
}

function dump($data){
	echo "<pre>";
	print_r($data);
	echo "</pre>";
}

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
}